%Sia dato il dataset ”Glass” che memorizza una matrice V di 214x9 valori reali che
%rappresentano 214 superfici di 7 tipi diversi di vetro misurate mediante 9 attributi. Tale
%dataset viene forntio tramite il file glass.zip che contiene una descrizione piú dettagli-
%ata del dataset (names.txt) e i dati numerici (data.txt).
%Si richiede di implementare in matlab una metodologia che consenta di trovare la
%migliore associazione algoritmo di clustering-distanza utilizzata, facendo uso di
%(a) Quattro algoritmi di clustering distinti, di cui almeno uno partizionale (es. kmeans).
%(b) Cinque distanze scelte a piacere, di cui almeno una euclidea.
%(c) Un indice di validazione che consenta di misurare la bontá del partizionamento
%ottenuto rispetto alla gold solution.
%In particolare, si richiede di (1) inglobare ognuna delle cinque distanze scelte su og-
%nuno dei quattro algoritmi di clustering (2) calcolare l’indice di validazione su ogni pos-
%sibile accoppiamento algoritmo-distanza considerando il dataset in oggetto come input.
%Si consideri che l’algoritmo k-means dipende dalla inizializzazione, pertanto occorre
%fornire il valore dell’indice di validazione mediato su piú esecuzioni dell’algoritmo.
%Fornire inoltre opportuni grafici esplicativi a supporto della soluzione trovata.


import_data

%From the file we know we have 7 clusters, one of them i empty
%   11. Type of glass: (class attribute)
%       -- 1 building_windows_float_processed
%       -- 2 building_windows_non_float_processed
%       -- 3 vehicle_windows_float_processed
%       -- 4 vehicle_windows_non_float_processed (none in this database)
%       -- 5 containers
%       -- 6 tableware
%       -- 7 headlamps

%V=PCAreduction(V);

%The clusters number is taken from the Gold Solution
%In this particular case we don't have the 4th type of glass.
G=unique(Gold);   %it will be 1,2,3,5,6,7  without 4
K=length(G);      %Number of clusters; it will be 6, not 7


%I choose the distances from the argument 'Distance' of the function
%pdist() I don't use 'minkowski' because with p=1 it is a cityblock, 
%with p=2 it is euclidean and with p=inf is chebychev.
distances={'Euclidean','Cityblock','Chebychev','Cosine','Correlation'};

%I use 3 agglomerative hierarchical algorithms: 
%single link, complete link e average link into the 'method'
% parameter of the linkage function
hierarchical={'Single','Complete','Average'};

%The best result with the Hierarchical Algorithms and the 5 distances
%will be stored here and updated each cycle
hbest={0,'NaN','NaN',{}};

%These are the 2 matrices for the bar plot
eval1=zeros(3,5);   %i,j
eval2=zeros(5,3);   %j,i

%tic
%------ HIERARCHICAL -------

for i=1:length(hierarchical)              % 3
    for j=1:length(distances)             % 5

        Z=linkage(V,hierarchical{i},distances{j});
        
        %builds the clusters from linkages, with K we specify the desired 
        %number of clusters
        T = cluster(Z,'MaxClust',K);

        %retrieves the Fowlkes-Mallows Index                                
        fm=FM(Gold,T);
        
        eval1(i,j)=fm;  %matrices for plot
        eval2(j,i)=fm;
        
        if fm > hbest{1}    %picks the max
            hbest{1}=fm;
            hbest{2}=distances{j};
            hbest{3}=hierarchical{i};
            hbest{4}=Z;
        end
      
    end
end
%toc



%------ K-MEDOIDS -------

%Because the outcome of the k-medoids algorithm depends on the initial
%cluster medoid positions, it will be executed N-times and among
%each evaluation will be made an average.
%"'Start','sample' Select k observations from X at random" to choose the 
%initial cluster medoid positions.
%I could use 'Replicates'Name-Value Pair Argument of kmedoids() function 
%to execute N-times with new random medoids and take the best execution, 
%but in this case we want an average with the Fowlkes-Mallows Index


%repeat the algorithm N-times
N=10;
%FM of kmedoids, average over N repetitions for each of the 5 distances
evalAVG=zeros(1,length(distances));

for n=1:N
    for i=1:length(distances)
        %"If the number of rows of X is less than 3000, 'pam' is the 
        %default algorithm."
        %"MaxIter Maximum number of iterations allowed. The default is 100"
        Y=kmedoids(V,K,'distance',distances{i},'Start','sample');
        
        %for each distance will make anaverage over N executions
        evalAVG(i)=evalAVG(i) + FM(Gold,Y)/N;
    end
end

%best pick for k-medoids algorithm
[M,I]=max(evalAVG);
kbest={M, distances{I},'k-medoids'};

% disp(evalAVG)


%------ HIERARCHICAL vs PARTITIONAL -------

%print the one with the highest FM index
if kbest{1}>hbest{1}
    fprintf('The best is %s with %s distance with FM=%f\n',...
        kbest{3}, kbest{2},kbest{1});
else
    fprintf('The best is %s Link with %s distance with Fowlkes-Mallows Index=%f\n'...
        ,hbest{3}, hbest{2},hbest{1});
    
    figure('Name','Dendrogram');
    %subplot(2,2,4);
    dendrogram(hbest{4})
    title([hbest{3} ' Link Dendrogram with ' hbest{2} ' distance' ])
end



%------ PLOTS ------

figure('Name','FM Hierarchical 1')
%subplot(2,2,1);
X = categorical({'Single Link','Complete Link','Average Link'});
X = reordercats(X,{'Single Link','Complete Link','Average Link'});
bar(X,eval1)
ylim([0 1])
title({'Fowlkes-Mallows Index for Hierarchical Algorithms';...
    'Distances grouped by algorihm'})
lgd = legend('Euclidean','Cityblock','Chebychev','Cosine',...
    'Correlation','Location','southeast');
title(lgd,'Distances:')

figure('Name','FM Hierarchical 2')
%subplot(2,2,2);
X = categorical({'Euclidean','Cityblock','Chebychev','Cosine',...
    'Correlation'});
X = reordercats(X,{'Euclidean','Cityblock','Chebychev','Cosine',...
    'Correlation'});
bar(X,eval2)
ylim([0 1])
title({'Fowlkes-Mallows Index for Hierarchical Algorithms';...
    'Algorithms grouped by distances'})
lgd = legend('Single Link','Complete Link','Average Link',...
    'Location','southeast');
title(lgd,'Hierarchical Alg:')



figure('Name','FM K-medoids')
%subplot(2,2,3);
X = categorical({'Euclidean','Cityblock','Chebychev','Cosine',...
    'Correlation'});
X = reordercats(X,{'Euclidean','Cityblock','Chebychev','Cosine',...
    'Correlation'});
b=bar(X,evalAVG);
title({'Fowlkes-Mallows Index for K-medoids Algorithm';...
    ['Calculated as averages over ' int2str(N) ' executions']});
ylim([0 1])
xtips1 = b(1).XEndPoints;
ytips1 = b(1).YEndPoints;
labels1 = string(b(1).YData);
text(xtips1,ytips1,labels1,'HorizontalAlignment','center',...
    'VerticalAlignment','bottom')

