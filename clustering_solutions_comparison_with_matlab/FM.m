function [FMindex] = FM(P,C)
%Fowlkes-Mallows Index
%It compare 2 clusters and retunrs a value, the closest is to 1 the better.
%
%INPUT: P,C are two N-by-1 arrays
%       P or C can be the direct output T from the cluster() function or
%       from the kmeans() function
%       E.g.
%           Z = linkage(X);
%           T = cluster(Z,'MaxClust',6);
%           fm=FM(Gold,T);              %where 'Gold' is the gold solution
%       
%
%OUTPUT: FMindex is a double value that represent the Fowlkes-Mallows index

%tic
% %Testing
%P=[1;2;1;1;1;2;3;4;4];
%C=[1;1;1;1;2;2;3;3;3];

N=length(P);
if N~=length(C)
    disp('The number of elements are different between the clusters')
    FMindex=NaN;
    return
end

%adjacency matrix for P and C
%because they are binary matrices I use smallest numeric data type.
MP=zeros(N,'uint8');
MC=zeros(N,'uint8');

%Building the MP and MC adjacency matrix relative to P and C
for i=1:N
    for j=1:N
        if i~=j && j>i                  %j>i upper triangle
            if P(i)==P(j)
                MP(i,j)=1;
            end
            if C(i)==C(j)
                MC(i,j)=1;
            end
        end
    end
end


%The Confusion Matrix with 4 variables instead of a 2-by-2 for easy reading
a00=0;
a01=0;
a10=0;
a11=0;

for i=1:N
    for j=1:N
        if i~=j && j>i                    %j>i scan only the upper triangle
            if MP(i,j)==0 && MC(i,j)==0
                a00=a00+1;
            elseif MP(i,j)==0 && MC(i,j)==1
                a01=a01+1;
            elseif MP(i,j)==1 && MC(i,j)==0
                a10=a10+1;
            else
                a11=a11+1;
            end
        end
    end
end
%A=[a00,a01;a10,a11]
Pr=a11/(a11+a01);
Re=a11/(a11+a10);
FMindex=sqrt(Pr*Re);
%toc

end
