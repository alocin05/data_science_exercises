clear
close all

%Sia dato il dataset ”Glass” che memorizza una matrice V di 214x9 valori 
%reali che rappresentano 214 superfici di 7 tipi diversi di vetro misurate
%mediante 9 attributi
%From the files we get:
% Attribute Information:
%    1. Id number: 1 to 214
%    2. RI: refractive index
%    3. Na: Sodium (unit measurement: weight percent in corresponding oxide, as 
%                   are attributes 4-10)
%    4. Mg: Magnesium
%    5. Al: Aluminum
%    6. Si: Silicon
%    7. K: Potassium
%    8. Ca: Calcium
%    9. Ba: Barium
%   10. Fe: Iron
%   11. Type of glass: (class attribute)


%Import the entire file
V=importdata('data.txt',',');

%The first column indicates indexes that we don't really need
%idx=V(:,1);

%The last column is the Gold Solution
Gold=V(:,end);

%Removes the 1st and the last column to take only the 9 attributes
V(:,1)=[];
V(:,end)=[];

