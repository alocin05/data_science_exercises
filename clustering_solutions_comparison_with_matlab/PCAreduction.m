function [ Xp ] = PCAreduction(X)
%Projects dataset X into a smaller-dimensional feature space, using the 
%eigenvector method of the covariance matrix of X

%Calculate the covariance matrix of dataset X
sigma=cov(X);

%Calculate the eigenvalues of the covariance matrix (symmetric)
[V,D]=eig(sigma);

%I extract the eigenvalues from D along the diagonal
d=diag(D);

%d vector with ordered eigenvectors,
%I contains the permutation of the indices of d that perform the ordering
[d,I]=sort(d,'descend');

%cumulative sums of the ordered eigenvalues
s=cumsum(d);
%sum of the eigenvalues
t=sum(d);
%divide the cumulative sum by the total
v=s/t;

%we find the number of components that preserve 95% of the variance
m=min(find(v>=0.95));

%matrice di passaggio
M=V(:,I(1:m));

%dataset X projected on the m-dimensional space Xp
Xp=X*M;

end

